<?php
namespace App\Controller;

use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Annotations as M;

class HomeController extends Controller
{
    /**
     * @M\Route("/")
     */
    public function homeAction()
    {
        return $this->render();
    }
}