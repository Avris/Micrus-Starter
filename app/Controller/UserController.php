<?php
namespace App\Controller;

use App\Form\LoginForm;
use App\Form\RegisterForm;
use App\Model\User;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Tool\Security\SecurityManager;

class UserController extends Controller
{
    /**
     * @M\Route("/login", name="login")
     */
    public function loginAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('account');
        }

        /** @var SecurityManager $sm */
        $sm = $this->get('securityManager');

        $loginForm = new LoginForm(new User(), $this->container);
        $loginForm->bind($this->getData());
        if ($loginForm->isValid()) {
            $sm->login($loginForm->getUser());

            $url = $this->getRequest()->getQuery('url');

            return $this->redirect($url ?
                $this->getRouter()->prependToUrl($url) :
                $this->generateUrl('account'));
        }

        $registerForm = new RegisterForm(new User(), $this->container);
        $registerForm->bind($this->getData());
        if ($registerForm->isValid()) {
            /** @var User $user */
            $user = $registerForm->getObject();
            $user->createAuthenticator(
                SecurityManager::AUTHENTICATOR_PASSWORD,
                $this->get('crypt')->hash($registerForm->getPassword())
            );
            $this->getEm()->persist($user);
            $this->getEm()->flush();
            $sm->login($user);

            return $this->redirectToRoute('account');
        }

        return $this->render([
            'loginForm' => $loginForm,
            'registerForm' => $registerForm,
        ]);
    }

    /**
     * @M\Route("/account", name="account")
     * @M\Secure
     */
    public function accountAction()
    {
        return $this->render();
    }
}
