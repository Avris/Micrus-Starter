<?php
namespace App\Form;

use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;

class RegisterForm extends Form
{
    public function configure() {
        $this
            ->add('username', Widget\Text::class, [
                'placeholder' => l('validator.UsernameFormat'),
            ], [
                new Assert\NotBlank(),
                new Assert\Regexp('^[A-Za-z0-9_]+$', l('validator.UsernameFormat')),
                new Assert\MinLength(5),
                new Assert\MaxLength(25),
                new Assert\Unique(
                    $this->container->get('orm'), $this->object, 'User', 'username',
                    l('validator.UsernameFree')
                ),
            ])
            ->add('email', Widget\Email::class, [], [
                new Assert\NotBlank(),
                new Assert\Unique(
                    $this->container->get('orm'), $this->object, 'User', 'email',
                    l('validator.EmailFree')
                ),
            ])
            ->add('doPasswordsMatch', Widget\ObjectValidator::class)
            ->add('password', 'Password', [],
                [new Assert\NotBlank(), new Assert\MinLength(5)]
            )
            ->add('passwordRepeat', Widget\Password::class, [], new Assert\NotBlank)
            ->add('agree', 'Checkbox', [
                'label' => '',
                'sublabel' => l('entity.User.register.agreement')
            ], new Assert\NotBlank)
        ;
    }

    public function doPasswordsMatch($user)
    {
        return $user->password === $user->passwordRepeat
            ? true
            : l('validator.PasswordRepeat');
    }

    public function getPassword()
    {
        return $this->object->password;
    }

    public function getName()
    {
        return 'Register';
    }
}
