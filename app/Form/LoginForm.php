<?php
namespace App\Form;

use App\Model\User;
use Avris\Micrus\Forms\Widget as Widget;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;

class LoginForm extends Form
{
    public function configure()
    {
        $this
            ->add('username', Widget\Text::class, [], new Assert\NotBlank())
            ->add('password', Widget\Password::class, [], [
                new Assert\NotBlank(),
                new Assert\CorrectPassword(
                    [$this, 'getUser'],
                    $this->container->get('crypt')
                )
            ])
        ;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->container->get('userProvider')->getUser($this->object->username);
    }

    public function getName()
    {
        return 'Login';
    }
}
