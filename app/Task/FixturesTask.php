<?php
namespace App\Task;

use App\Model\User;
use Avris\Micrus\Doctrine\DoctrineFixturesTask;
use Avris\Micrus\Tool\Security\SecurityManager;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesTask extends DoctrineFixturesTask
{
    protected function load(OutputInterface $output)
    {
        $output->writeln('Truncating the database');
        $this->truncateDatabase();

        $crypt = $this->container->get('crypt');

        $output->writeln('Loading: users');

        $admin = new User();
        $admin->setUsername('admin');
        $admin->setEmail('admin@micrus.avris.it');
        $admin->setRole('ROLE_ADMIN');
        $admin->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('admin'));
        $this->em->persist($admin);

        $user = new User();
        $user->setUsername('user');
        $user->setEmail('user@micrus.avris.it');
        $user->setRole('ROLE_USER');
        $user->createAuthenticator(SecurityManager::AUTHENTICATOR_PASSWORD, $crypt->hash('user'));
        $this->em->persist($user);

        $this->em->flush();

        $output->writeln('Fixtures loaded');
    }
}

