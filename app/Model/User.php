<?php
namespace App\Model;

use Avris\Micrus\Model\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 **/
class User implements UserInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    public static $roles = [
        self::ROLE_USER => 'user',
        self::ROLE_ADMIN => 'admin',
    ];

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @var Authenticator[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Authenticator", mappedBy="user", cascade={"persist", "remove"})
     **/
    protected $authenticators;

    public function __construct()
    {
        $this->authenticators = new ArrayCollection();
        $this->role = self::ROLE_USER;
    }


    public function getIdentifier()
    {
        return $this->username;
    }

    public function getAuthenticators($type = null)
    {
        return $this->authenticators->filter(function (Authenticator $auth) use ($type) {
            $typeValid = $type ? $auth->getType() === $type : true;
            return $auth->isValid() && $typeValid;
        });
    }

    public function createAuthenticator($type, $payload, \DateTime $validUntil = null)
    {
        $auth = new Authenticator();
        $auth->setType($type);
        $auth->setPayload($payload);
        $auth->setUser($this);
        $auth->setValidUntil($validUntil);

        $this->authenticators->add($auth);

        return $auth;
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return [$this->role];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return self::$roles[$this->role];
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function __toString()
    {
        return $this->username;
    }
}
