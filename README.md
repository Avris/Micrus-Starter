## Micrus Starter Project ##

This is a starter project for the [Micrus framework](https://micrus.avris.it).

Its documentation is available at **[docs.avris.it/micrus](https://docs.avris.it/micrus/)**.

Also, check out the **[Demo project](https://gitlab.com/Avris/Micrus-Demo)**.

### Instalation ###

Install [Composer](https://getcomposer.org/download/), move to the web directory of your server and run:

    composer create-project avris/micrus-starter project-name

Composer will download the project with all of its dependencies.
It will ask you for server parameters (like database connection)
and will generate a secret encryption key unique for your app.

To generate database schema and load sample data, run:

    bin/micrus db:schema:create
	bin/micrus db:fixtures

And that's it. Your app is available under URL:

	http://localhost/project-name/web/app_dev.php

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
